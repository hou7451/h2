
export function buff_e() {
    if (!localStorage.getItem("buff-again")) {
        var data = {
            bottom: 0,
            top: 1,
            unity: 0,
            normal: 0
        }
    }else{
        var data=JSON.parse(localStorage.getItem("buff-again"));
        data["top"]=parseInt(data["top"])+1;
    }
    localStorage.setItem(JSON.stringify(data));
}

export function add_e() {
    if (parseFloat(localStorage.getItem("alpha")) <= -8) {
        dialogAlert("您的alpha值低于-6，不满足购买条件");
        return
    }
    money(-25)
    alpha(3);
    itemadd("D0");
}
export function remove_e() {
    dialogAlert("移除成功");
}