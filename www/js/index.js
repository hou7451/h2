var info, sp1c, sp2c, sp3c, ctd, httpd = null; //拟态框,大学模块,倒计时,httpd
function initia() { //初始化渲染页面

    //初始化
    sp1c = '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">滑稽信息</span><p>您现在正在使用滚动更新版本，可能会遇上未检测出的bug，主程序版本号Ver1.0.1.2，数据版本5.15ADS</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2">来看看源代码</a><a href="https://afdian.net/@acted">爱发电支持下！</a><br><a href="https://t.me/joinchat/MUKY6Ej2AvLJEG2JigyE8A">telegram群</a></div></div></div>';
    sp2c = '';
    sp3c = '';
    //开始检查课程文件
    switch (localStorage.getItem("uni")) { //动态的装载进度文件
        case "1": {//忠贞之夜
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable pink"><span class="white-text">您正在游玩第一章：忠贞之夜</span></div></div>'
            sp2c += '<div class="col s6 m3 l2"><div class="card-panel hoverable red"><span class="white-text">商店与效果系统在第一章不可用</span></div></div>'
            sp3c += '<div class="col s6 m3 l2"><div class="card-panel hoverable red"><span class="white-text">住所系统在第一章不可用</span></div></div>';

            //检查当前进度，获取课程表信息。
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        let json = JSON.parse(xhr.responseText);
                        let code = localStorage.getItem("cls");
                        if (code == undefined) {
                            code = "U0_0";
                            localStorage.setItem("cls", "U0_0");
                        }
                        let clsn = json[code];
                        sp1c += '<div class="col s6 m3 l2"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" src="./img/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i                    class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U0/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>一切，都是从那一天起。</p><p>第一章：忠贞之路</p><p>课程代码' + code + '</p></div></div></div>';


                        //帮助信息
                        sp1c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">帮助信息</span><p>Hey！不太了解怎么使用？那就来看看帮助文件吧！</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2/wikis/%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%BF%A0%E8%B4%9E%E4%B9%8B%E5%A4%9C">帮助信息</a></div></div></div>'
                        //玩具大小
                        sp1c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">玩具大小</span><p>之前忘了写了，除了在“回家路上”关卡使用中型假阳具以外</p><p>第一章其余部分使用小号阳具</p><p>阳具大小可在wiki上找到</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2/wikis/%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%BF%A0%E8%B4%9E%E4%B9%8B%E5%A4%9C">Wiki!!!</a></div></div></div>';
                        document.getElementById("test-swipe-1").innerHTML = '<div class="row">' + sp1c + '</div>'
                        initgo();
                    }
                    else {
                        dialogAlert("网络错误，代码" + xhr.status);
                    }
                }
            }
            xhr.open("get", "./CLS.json");
            xhr.send();
            break;
        }
        case "2": {//那天之后
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable pink"><span class="white-text">您正在游玩第二章：那天之后</span></div></div>'
            sp2c += '<div class="col s6 m3 l2"><div class="card-panel hoverable red"><span class="white-text">商店与效果系统尚未制作</span></div></div>'
            sp3c += '<div class="col s6 m3 l2"><div class="card-panel hoverable red"><span class="white-text">住所系统在第二章不可用</span></div></div>';

            //检查当前进度，获取课程表信息。
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        let json = JSON.parse(xhr.responseText);
                        let code = localStorage.getItem("cls");
                        if (code == undefined) {//使用了旧版本程序
                            modal("U1error");
                        }
                        let clsn = json[code];
                        sp1c += '<div class="col s6 m3 l2"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" src="./img/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i                    class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U1/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第二章：那天之后</p><p>课程代码' + code + '</p></div></div></div>';


                        //帮助信息
                        sp1c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">帮助信息</span><p>Hey！不太了解怎么使用？那就来看看帮助文件吧！</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2/wikis/%E7%AC%AC%E4%B8%80%E7%AB%A0%EF%BC%9A%E5%BF%A0%E8%B4%9E%E4%B9%8B%E5%A4%9C">帮助信息</a></div></div></div>'
                        document.getElementById("test-swipe-1").innerHTML = '<div class="row">' + sp1c + '</div>'
                        initgo();
                    }
                    else {
                        dialogAlert("网络错误，代码" + xhr.status);
                    }
                }
            }
            xhr.open("get", "./CLS.json");
            xhr.send();
            break;
        }
        case "gameover": {//游戏结束（异常）
            sp1c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
            sp2c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
            sp3c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
            initgo(); //进入初始化第二
            break;
        }
        default: { //啥也没有
            sp1c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">滑稽信息</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(1)">开始游戏</a></div></div></div><div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">帮助信息</span><p>Hey！不太了解怎么使用？那就来看看帮助文件吧！</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2/wikis/home">帮助页面</a></div></div></div>';
            sp2c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">滑稽信息</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(1)">开始游戏</a></div></div></div>';
            sp3c += '<div class="col s6 m3 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">滑稽信息</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(1)">开始游戏</a></div></div></div>';
            initgo(); //进入初始化第二
            break;
        }
    }

}

function initgo() { //初始化第二


        //伴侣信息、alpha,sissy,slut，MONEY值
        if(localStorage.getItem("couple")){
            var couple;
            switch (localStorage.getItem("couple")) {
                case ("0"): {
                    couple = "你的伴侣是♀-莉娅"
                    break;
                }
                case ("1"): {
                    couple = "你的伴侣是⚧-米娅"
                    break;
                }
                default: {
                    couple = "你尚未选择伴侣或存储信息有误"
                }
            }
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable purple accent-2"><span class="white-text">' + couple + '</span></div></div>';
            couple = undefined;
        }

    
        if (localStorage.getItem("alpha")) {
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable blue accent-2"><span class="white-text">你的Alpha值为' + localStorage.getItem("alpha") + '</span></div></div>';
        }
        if (localStorage.getItem("sissy")) {
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable pink accent-1"><span class="white-text">你的sissy值为' + localStorage.getItem("sissy") + '</span></div></div>';
        }
        if (localStorage.getItem("slut")) {
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable deep-orange accent-2"><span class="white-text">你的slut值为' + localStorage.getItem("slut") + '</span></div></div>';
        }
        if (localStorage.getItem("money")) {
            sp1c += '<div class="col s6 m3 l2"><div class="card-panel hoverable deep-orange accent-2"><span class="white-text">你有' + localStorage.getItem("money") + '元</span></div></div>';
        }


    //广告ads
    sp1c += '<div class="col s12 m6 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">广告</span><ins id="782414" data-width="250" data-height="262"></ins></div></div></div>';
    sp2c += '<div class="col s12 m6 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">广告</span><ins id="782417" data-width="250" data-height="262"></ins></ins></div></div></div>';
    sp3c += '<div class="col s12 m6 l2"><div class="card hoverable"><div class="card-content"><span class="card-title">广告</span><ins id="782418" data-width="250" data-height="262"></ins></div></div></div>';



    var sp1 = document.getElementById("test-swipe-1"),
        sp2 = document.getElementById("test-swipe-2"),
        sp3 = document.getElementById("test-swipe-3");

    //渲染到DOM
    sp1.innerHTML = '<div class="row">' + sp1c + '</div>'
    sp2.innerHTML = '<div class="row">' + sp2c + '</div>'
    sp3.innerHTML = '<div class="row">' + sp3c + '</div>'
    document.getElementById("loading").style.display = "none"

    //初始化标签页们
    let alltab = document.getElementsByClassName("tatatabs");
    let t1 = 0,
        a1 = alltab.length;
    while (t1 <= a1) {
        M.Tabs.init(alltab[t1]);
        t1 += 1;
    }
}


var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        //检查数据版本是否兼容,不兼容则执行升级
        if (localStorage.getItem("DataVersion")) {
            if (parseInt(localStorage.getItem("DataVersion")) < 5) {
                dialogAlert("数据版本较低，将执行升级");
                window.location.replace("upgrade/" + localStorage.getItem("DataVersion") + ".html");
            }
        } else {
            localStorage.setItem("DataVersion", 5)
        }

        //如果为Android或IOS平台则初始化httpd
        if (device.platform == "Android" || device.platform == "iOS") {
            httpd = (cordova && cordova.plugins && cordova.plugins.CorHttpd) ? cordova.plugins.CorHttpd : null;
        }
        if (httpd) {
            startServer("js/module");
        }

        //初始化标签页
        M.Tabs.init(document.getElementById("slide"), {
            swipeable: false //还是把滑动关掉好了
        });
        M.Sidenav.init(document.querySelectorAll('.sidenav'));
        info = M.Modal.init(document.getElementById("modal"), {});
        //调整内容系统高度，仅在启用滑动时有效
        /*document.getElementById("tabs").style.height = window.innerHeight - 112 + "px";
        document.getElementsByClassName("tabs-content")[0].style.height = "inherit";*/

        //初始化DOM内容
        initia();

        //课程继续
        if (localStorage.getItem("clsup")) {
            dialogAlert("看起来RBQ正在上课呢！");
            clsrestart();
        }
    }
};

app.initialize();