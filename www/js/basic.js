function dialogAlert(message, title, buttonname, callback) { //通知服务
    title = title || "错误";
    buttonname = buttonname || "确定";
    callback = callback || function () {
        return;
    }
    if (navigator.notification) {
        navigator.notification.alert(message, callback, title, buttonname);
    } else {
        alert(message);
    }
}
function loc(ati) { //动画跳转
    document.body.addEventListener("animationend", function () {
        document.location = ati;
    }.bind(this))
    document.body.style.animation = "hidden 0.3s forwards";
}


function ctdgo() { //各类倒计时
    if (!localStorage.getItem("timeset")) {
        window.clearInterval(ctd);
        return;
    }

    let d = new Date(),
    left = Math.ceil((parseInt(localStorage.getItem("timeset")) - parseInt(d.getTime())) / 1000)
    document.getElementById("CountDown").innerHTML = "距离结束还有" + left + "秒"
    if (left < 1) {
        window.clearInterval(ctd);
        document.getElementById("CountDown").innerHTML = "";
        var allcls = document.getElementsByClassName("timeset");
        var t = 0, a = allcls.length;
        while (t <= a && allcls[t]!=undefined) {
            allcls[t].classList.remove("disabled");
            t += 1
        }
    }
}

function clean() { //清除localstorage信息
    localStorage.clear();
    location.reload();
}

function modal(node) { //拟态框页面
    var xhr = new XMLHttpRequest();
    xhr.open("GET", './modals/' + node + '.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                let ans = JSON.parse(xhr.responseText);
                document.getElementById("info_header").innerHTML = ans.header;
                document.getElementById("info_content").innerHTML = ans.content;
                document.getElementById("info_footer").innerHTML = ans.footer;
                info.open();
            } else {
                dialogAlert("网络错误，代码" + xhr.status);
            }
        }
    }
}

function meowout(){//导出数据，MeowVer1

        var t=0,data={},array=["uni","cls","alpha","slut","sissy","couple"];
        while(t<array.length){
            if(localStorage.getItem(array[t])){
                data[array[t]]=localStorage.getItem(array[t]);
            }
            t+=1;
        }
    data["ver"]="MeowVer1"

    document.getElementById("info_header").innerHTML ="数据导出";
    document.getElementById("info_content").innerHTML = ' <textarea>'+JSON.stringify(data)+'</textarea>';
    document.getElementById("info_footer").innerHTML = '<a href="#!" class="modal-close waves-effect waves-green btn-flat" >关闭</a>';
    setTimeout(() => {
        info.open();
    }, 10);
}

function meowin(){
    document.location="meowin.html?meowin="+document.getElementById("meowin").value;
}